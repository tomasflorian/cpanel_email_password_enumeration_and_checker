#!/bin/bash

while PASS= read -r line
do
        echo "abort? ctrl-C"
        sleep 2
        echo $line
        ./check.py $line > results
        cat results | grep FAIL

done < passwords


