opendir(USERS, '/var/cpanel/users') || die $!;
while (my $user = readdir(USERS)) {
    # user loop
    next if $user =~ /^\.|system/; # skip . and .. and system dirs
    if (opendir(ETC, "/home/$user/etc")) {
    while (my $domain = readdir(ETC)) {
        next if $domain =~ /^\./; # skip . and .. dirs
        if (-d "/home/$user/etc/$domain/") {
             if (opendir(MAIL, "/home/$user/mail/$domain/")) {
             while (my $email = readdir(MAIL)) {
                next if $email =~ /^\./;
                if (-d "/home/$user/mail/$domain/$email/") {
                #print "$user    $email\@$domain    ";
                print "$email\@$domain\n";
                #system("du -BM --max-depth=0 /home/$user/mail/$domain/$email/ | cut -f1");
                }
                }
        }
        closedir(MAIL); }
    }
    closedir(ETC); }
}
closedir(USERS);
